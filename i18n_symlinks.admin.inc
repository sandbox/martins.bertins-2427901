<?php

/**
 * @file
 * Administration pages for node symlinks translation.
 */

/**
 * Node symlinks overview page
 */
function i18n_symlinks_admin_overview($type = NULL) {
  module_load_include('admin.inc', 'i18n_translation');
  return i18n_translation_admin_overview('symlinks');
}

/**
 * Node symlinks add/edit form
 */
function i18n_symlinks_admin_form($form, $form_state, $translation_set = NULL) {
  $form['translation_set'] = array('#type' => 'value', '#value' => $translation_set);
  if ($translation_set) {
    $paths = $translation_set->get_translations();
    $tsid = $translation_set->tsid;
  }
  else {
    $paths = array();
    $tsid = NULL;
  }
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => $translation_set ? $translation_set->title : '',
    '#description' => t('Optional descriptive name for this set.'),
  );
  $form['translations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Translations'),
    '#tree' => TRUE,
  );
  
  // Fetch all available symlinks.
  $symlinks = _i18n_symlinks_get_symlinks($tsid);

  foreach (i18n_language_list() as $langcode => $name) {
    $form['translations'][$langcode] = array(
      '#type' => 'select',
      '#title' => check_plain($name),
      '#options' => array('none' => t('Select')) + $symlinks[$langcode],
      '#default_value' => !empty($paths[$langcode]) ? $paths[$langcode]->path : '',
    );
  }

  $form['controls']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#name' => 'save',
  );

  if ($translation_set) {
    $form['controls']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#name' => 'delete',
    );
  }
  return $form;
}

/**
 * Process form submission
 */
function i18n_symlinks_admin_form_submit($form, &$form_state) {
  $translation_set = $form_state['values']['translation_set'];
  switch ($form_state['triggering_element']['#name']) {
    case 'save':
      $paths = array_filter($form_state['values']['translations']);
      $translation_set = $translation_set ? $translation_set : i18n_translation_set_create('symlinks');
      $translation_set->title = '';
      $translations = $translation_set->get_translations();
      foreach ($paths as $lang => $path) {
        if (isset($translations[$lang])) {
          $translations[$lang]->path = $path;
        }
        else {
          $translation_set->add_item($path, $lang);
        }
      }

      foreach (array_diff(array_keys($translation_set->get_translations()), array_keys($paths)) as $language) {
        $translation_set->remove_language($language);
        unset($translations[$language]);
      }

      if (!empty($form_state['values']['title'])) {
        $translation_set->title = $form_state['values']['title'];
      }

      $translation_set->save(TRUE);
      drupal_set_message(t('The node symlinks translation has been saved.'));
      break;
    case 'delete':
      $destination = array();
      if (isset($_GET['destination'])) {
        $destination = drupal_get_destination();
        unset($_GET['destination']);
      }
      $form_state['redirect'] = array($translation_set->get_delete_path(), array('query' => $destination));
      return;
  }
  $form_state['redirect'] = 'admin/config/regional/i18n_translation/symlinks';
}

/**
 * Returns full menu path.
 */
function _i18n_symlinks_full_path($mlid, &$path = array()) {
  $query = db_select('menu_links', 'ml');
  $query->fields('ml', array('link_title', 'plid'));
  $query->condition('ml.mlid', $mlid);
  $result = $query->execute()->fetch();
  array_unshift($path, $result->link_title);
  if ($result->plid > 0) {
    $path = _i18n_symlinks_full_path($result->plid, $path);
  }
  return $path;
}

/**
 * Process form validation
 */
function i18n_symlinks_admin_form_validate($form, &$form_state)  {
  if ($form_state['triggering_element']['#name'] == 'save') {
    $paths = &$form_state['values']['translations'];
    // No symlinks have been selected.
    if (count(array_unique($paths)) === 1) {
      form_set_error('paths', t('No symlinks selected.'));
    }
  }
}

/**
 * Assemble symlinks list to use in the add/edit form selects.
 */
function _i18n_symlinks_get_symlinks($tsid) {
  // Get all symlinks.
  $symlinks = array();
  $query = db_select('nodesymlinks_link_storage', 'nls');
  $query->fields('nls', array('item_data', 'nid'));
  $symlink_results = $query->execute()->fetchAll();
  
  // Get symlinks that are used on other translation sets.
  $query = db_select('i18n_symlinks', 'foo');
  $query->fields('foo', array('path'));
  if ($tsid) {
    $query->condition('foo.tsid', $tsid, "<>");
  }
  $results = $query->execute();
  while ($path = $results->fetchField()) {
    $exclude_symlinks[] = $path;
  }
  
  foreach ($symlink_results as $key => $result) {
    $data = unserialize($result->item_data);
    // Don't show symlinks that are used in other translation sets.
    if (!in_array($data['link_path'], $exclude_symlinks)) {
      // Get language from original node
      $query = db_select('node', 'n');
      $query->fields('n', array('language'));
      $query->condition('n.nid', $result->nid);
      $language = $query->execute()->fetchField();
      // Get full menu path
      $full_path = _i18n_symlinks_full_path($data['mlid']);
      $symlinks[$language][$data['link_path']] = implode(' -> ', $full_path);
    }
  }
  
  return $symlinks;
}
